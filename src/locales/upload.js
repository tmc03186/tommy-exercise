import EN from './en/upload'
import CN from './zh-cn/upload'
import TW from './zh-tw/upload'
export default {
  en: EN,
  'zh-cn': CN,
  'zh-tw': TW
}
