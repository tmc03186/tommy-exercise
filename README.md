### Setup
yarn

### Development
yarn dev

### Production
yarn build
yarn start

### Export Static
yarn export

### Serve Static
yarn serve

### Lint
yarn lint

### Test
yarn test

#### Sample .env
```bash
API_ENDPOINT=https://api.altizure.com/graphql
API_KEY=sh58LhmAy073HMFaKYU1EoyhvqW1jPhkxZJVetc

OAUTH_ENDPOINT=https://api.altizure.com/auth
OAUTH_CALLBACK=http://localhost:3000/login-callback
```
