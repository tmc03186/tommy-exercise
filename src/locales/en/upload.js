export default {
  title: 'Upload images to',
  titleMeta: 'Upload meta files to project',
  learnMore: 'Learn more',
  free: 'Free Project',
  paid: 'Professional Project',
  uploader: {
    drop: 'Click or Drop files',
    edge: 'Cloud Edge',
    init: 'Initializing',
    uploading: 'Uploading',
    obtainingSTS: 'Connecting',
    skipping: 'Skipping',
    registering: 'Registering',
    uploaded: 'Uploaded',
    error: 'Error'
  },
  reminder: {
    tip: 'Make sure your images meet the following requirement for the best result.',
    dos: {
      'overlap': 'Sufficient overlapping',
      'angle': 'Contain different angled shot',
      'color': 'Consistent camera setting',
      'static': 'Static object'
    },
    donts: {
      'surface': 'Transparent, reflective, textureless surface',
      'blur': 'Blury, out-of-focus images',
      'mark': 'Timestamp or other fixed marks'
    },
    note: 'Note',
    note0: 'Resolution lower than 8.0MP will be counted as 8.0MP to encourage uploading high quality images.',
    note1: 'Support resume uploading for identical file. Uploaded files will be skipped',
    note2: 'Support uploading different files with duplicated name. For example, DJI reset file name for every 1000 images. Uploading them all will not overwrite each other.',
    note3: 'We recommend uploading no more than 1000 images in each batch. Otherwise your browser may crash.',
    note4: 'Data capturing tutorial',
    note4Link: `/support/articles/tutorial_capture`
  },
  progress: 'Progress',
  total: 'All files',
  uploading: 'Uploading',
  processing: 'Processing',
  // processing: 'Processing',
  valid: 'Valid',
  invalid: 'Invalid',
  detail: 'Detail',
  more: 'more',
  no: 'None',
  image: {
    title: 'Image List',
    hasProcessing: 'Some images are still processing in the cloud.',
    confirmRemove: 'Confirm to remove all selected images ? Total',
    perpage: 'Per page',
    select: 'Select',
    confirm: 'Confirm',
    removing: 'Removing',
    cancel: 'Cancel',
    loadMore: 'Load more',
    width: 'Width',
    height: 'Height',
    filesize: 'Filesize',
    datasize: 'Datasize',
    gp: 'GigaPixel',
    unique: 'Unique Name',
    checksum: 'Checksum',
    remove: 'Remove',
    state: {
      title: 'State',
      All: 'All',
      Pending: 'Pending', // validated with OSS
      Uploading: 'Uploading', // uploading to OSS
      Uploaded: 'Uploaded', // uploaded to OSS
      Verifying: 'Verifying', // after uploaded to OSS
      Valid: 'Valid', //  after verfiying and OK
      Invalid: 'Invalid', // after and verfied
      Copying: 'Copying', // copying from OSS to local
      Ready: 'Ready' // copied to local
    }
  },
  report: 'Report problem',
  retry: 'Retry All',
  switch: 'Try different server',
  // starttask
  gcp: 'Enable Ground Control Point',
  autoStart: 'Auto-start reconstruction process after upload is done',
  autoStartTip: 'It starts when Uploading files reach 0',
  push: 'Start',
  next: 'Next step',
  // alert message
  alert: {
    refreshAlert: {
      title: 'Images Updated',
      message: 'Please refresh page'
    },
    removeError: {
      title: 'Remove failed',
      message: 'Image cannot be removed at the moment.'
    },
    pushError: {
      title: 'Server Error',
      message: 'Reconstruction cannot be started at the moment. Fortunately, your uploaded images are safe. Please try again later.'
    },
    // see graphql doc
    pushError0: {
      title: 'Authentication error',
      message: 'Please login to your account to start reconstruction.'
    },
    pushError1: {
      title: 'Project not found',
      message: 'Reconstruction cannot be started.'
    },
    pushError2: {
      title: 'Image not ready',
      message: 'Some images are still processing in the cloud. Project can only be started when all images are ready.',
      action: 'See all image'
    },
    pushError3: {
      title: 'Reconstruction already started',
      message: 'You can stop the current reconsturction if needed.'
    },
    pushError4: {
      title: 'Insufficient fund',
      message: 'Please puchase enough Alticoin to start reconstructing professional projects.',
      action: 'Purchase'
    },
    pushError5: {
      title: 'Images not changed',
      message: 'Input images are not changed since last reconstruction. Restarting will not change the result.'
    },
    pushError6: {
      title: 'Free quota exceeded',
      message: 'Project will not be scheduled for reconstruction.'
    },
    pushError7: {
      title: 'Not enought images',
      message: 'Please upload more images.'
    },
    pushError8: {
      title: 'Invalid coupon',
      message: 'Coupon cannot be used.'
    },
    pushError9: {
      title: 'Coupon expired',
      message: 'Coupon cannot be used.'
    },
    uploadError: {
      title: 'Upload Error',
      message: 'Files cannot be uploaded at the moment. Please try again later.'
    },
    reportError: {
      title: 'Report Problem Error',
      message: 'Problem cannot be reported at the moment. Please try again later or report the problem to us at support@altizure.com'
    },
    reportDone: {
      title: 'Problem Reported',
      message: 'Thanks for your report. We will investigate the problem and get back to you soon.'
    },
    upgradeError: {
      title: 'Server Error',
      message: 'Project cannot be upgraded to professional at the moment'
    },
    upgradeError0: {
      title: 'Authentication error',
      message: 'Please login to your account to upgrade the project.'
    },
    upgradeError1: {
      title: 'Project not found',
      message: 'Cannot upgrade.'
    },
    upgradeError2: {
      title: 'Already upgraded',
      message: 'Project is already professional. No need to upgrade.'
    },
    upgradeError3: {
      title: 'Some task is running',
      message: 'Please wait until the current task finishes.'
    },
    upgradeError4: {
      title: 'Insufficient fund',
      message: 'Please puchase enough Alticoin to upgrade the project.',
      action: 'Purchase'
    }
  },
  overlay: {
    title: 'Almost there...',
    message: 'Due to some technical issue, you have to manually start GCP reconstruction in project overview page yourself.',
    ok: 'Go',
    cancel: 'Cancel'
  }
}
