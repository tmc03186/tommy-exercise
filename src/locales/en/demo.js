export default {
  title: 'Demo',
  home: 'Home',
  altizure: 'altizure.com',
  notifySuccess: 'Dispatch Success Alerts',
  notifyError: 'Dispatch Failed Alerts'
}
