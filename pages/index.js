import { App, GradientButton, LoadingWidget, StarIcon, color, theme, withData, withi18nSSR } from '@ald/ui'
import { Flex, Text } from 'rebass'
import React, { Component } from 'react'
import { error, success } from '../src/alerts/demo'

import CheckingContent from '../src/components/checking-content'
import DemoHeader from '../src/components/demo-header'
import PropTypes from 'prop-types'
import demo from '../src/locales/demo'
import { graphql } from 'react-apollo'
import query from '../src/gql/queries/my-self'

@withData
@withi18nSSR(demo)
@graphql(query)
class WelcomePage extends Component {
  static contextTypes = {
    store: PropTypes.object
  }
  static propTypes = {
    i18n: PropTypes.object,
    data: PropTypes.object
  }
  notify = (type) => {
    const { i18n: { lang } } = this.props
    const { store: { dispatch } } = this.context
    dispatch(type === 'success' ? success(lang) : error(lang))
  }
  notifyError = () => this.notify('error')
  notifySuccess = () => this.notify('success')
  render () {
    const { i18n, data } = this.props
    return (
      <App title='Altizure | SPA Starter' customHeader={<DemoHeader />} option={{native: false}}>
        <CheckingContent fileName='foo' />
        {/*
          <Flex w={theme.contentWidth} m='auto' column align='center'>
            <Flex align='center' m={5} f={30}>
              <StarIcon size={30} color={color.yellow} />
              <Text ml={2} children={i18n.title} />
            </Flex>
            <GradientButton
              onClick={this.notifySuccess}
              children={i18n.notifySuccess} />
            <GradientButton
              mt={3}
              bg={color.red}
              onClick={this.notifyError}
              children={i18n.notifyError} />
            {data.loading ? (
              <LoadingWidget />
            ) : (
              <Text m={3} children={`my name is ${data.my.self.name}`} />
            )}
          </Flex>
        */}
      </App>
    )
  }
}

export default WelcomePage