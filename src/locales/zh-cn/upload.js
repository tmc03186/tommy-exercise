export default {
  title: '上传照片到',
  titleMeta: '上传辅助文件',
  learnMore: '了解更多',
  free: '免费项目',
  paid: '专业项目',
  uploader: {
    drop: '点击或拖拉文件',
    edge: '云端位置',
    init: '初次化中',
    uploading: '正在上传',
    obtainingSTS: '连接云端',
    skipping: '跳过已上传文件',
    registering: '核对文件中',
    uploaded: '上传成功',
    error: '出错文件'
  },
  reminder: {
    tip: '请确保图片满足以下条件，以达最佳效果。',
    dos: {
      'overlap': '足够重叠度',
      'angle': '多角度拍摄',
      'color': '统一相机设定',
      'static': '物件静止'
    },
    donts: {
      'surface': '透明，反光，纯色表面',
      'blur': '模糊，失焦图片',
      'mark': '时间印或其他故定标记'
    },
    note: '提示',
    note0: '照片不足800萬像素會當作800萬像素計算。 希望用戶上傳高質量照片。',
    note1: '支援断续上传内容一样的文件，会自动跳过已上传的文件。',
    note2: '支援上传同名字但不同内容的文件。只要文件内容不一样，系统亦视作不同文件来上传。例如 DJI 的照片每 1000 张便重复名字，但一起上传亦不会互相覆盖。',
    note3: '我们建议每次上传不多过 1000 张照片。否则浏览器可能会出错。',
    note4: '数据采集​​教学',
    note4Link: `/support/articles/tutorial_capture`
  },
  progress: '进度',
  total: '所有文件',
  uploading: '上传中',
  processing: '处理中',
  // processing: '上传中',
  valid: '有效文件',
  invalid: '无效文件',
  detail: '详情',
  more: '还有更多',
  no: '沒有',
  image: {
    title: '项目照片',
    hasProcessing: '尚有照片在云端处理中。',
    confirmRemove: '确认删除所选文件？数量一共',
    perpage: '每页显示',
    select: '选择',
    confirm: '确认',
    removing: '删除中',
    cancel: '取消',
    loadMore: '更多',
    width: '宽',
    height: '高',
    filesize: '文件大小',
    datasize: '数据大小',
    gp: '十亿像素',
    unique: '独有名字',
    checksum: '校验码',
    remove: '删除',
    state: {
      title: '文件状态',
      All: '全部',
      Pending: '等候上传', // validated with OSS
      Uploading: '上传中', // uploading to OSS
      Uploaded: '上传完成', // uploaded to OSS
      Verifying: '检查中', // after uploaded to OSS
      Valid: '有效照片', //  after verfiying and OK
      Invalid: '无效文件', // after and verfied
      Copying: '传送到重建系统', // copying from OSS to local
      Ready: '全部完成' // copied to local
    }
  },
  report: '报告问题',
  retry: '重试全部',
  switch: '换伺服器试试',
  // starttask
  gcp: '加入控制点 (GCP)',
  autoStart: '处理完成所有文件后自动开始重建程序',
  autoStartTip: '即 『上传中』的文件数量降至 0 时开始',
  push: '开始重建',
  next: '下一步',
  // alert message
  alert: {
    refreshAlert: {
      title: '照片已更新',
      message: '请刷新页面'
    },
    removeError: {
      title: '删除失败',
      message: '照片无法删除，请稍候再试。'
    },
    pushError: {
      title: '系统错误',
      message: '重建任务现时无法开始。请放心，你已上传的图片没有丢失，请稍候重试。'
    },
    // see graphql doc
    pushError0: {
      title: '帐户认证错误',
      message: '请先登录你的帐户。'
    },
    pushError1: {
      title: '找不到项目',
      message: '项目不存在，无法启动重建。'
    },
    pushError2: {
      title: '照片未准备好',
      message: '尚有照片在云端处理中。当所有照片从云端传送到运算机器，方能启动重建。',
      action: '查看所有照片'
    },
    pushError3: {
      title: '重建已经开始',
      message: '如有需要，请先停止目前的重建。'
    },
    pushError4: {
      title: '余额不足',
      message: '请先购买足够 Alticoin ，方可为专业项目启动重建。',
      action: '购买'
    },
    pushError5: {
      title: '项目没有改变',
      message: '如果输入数据没有改变，再次重建也不会改变。'
    },
    pushError6: {
      title: '超出免费额度',
      message: '重建无法启动。'
    },
    pushError7: {
      title: '照片不足',
      message: '請上傳更多照片。'
    },
    pushError8: {
      title: '优惠券无效',
      message: '无法使用优惠券。'
    },
    pushError9: {
      title: '优惠券过期',
      message: '无法使用优惠券。'
    },
    uploadError: {
      title: '上传错误',
      message: '文件暂时无法上传，请稍候重试。'
    },
    reportError: {
      title: '报告问题出错',
      message: '问题暂时无法报告给我们，请你稍后再试。或者把直接问题发到support@altizure.com。谢谢！'
    },
    reportDone: {
      title: '报告成功',
      message: '谢谢你的报告。我们会尽快调查跟进，与你联络。'
    },
    upgradeError: {
      title: '系统错误',
      message: '项目暂时无法升级成专业，请稍候重试。'
    },
    upgradeError0: {
      title: '帐户认证错误',
      message: '请先登录你的帐户。'
    },
    upgradeError1: {
      title: '找不到项目',
      message: '无法升级成专业。'
    },
    upgradeError2: {
      title: '项目已经升级',
      message: '项目已经是专业，毋须再升级。'
    },
    upgradeError3: {
      title: '项目有正在执行的任务',
      message: '请先等候任务完成。'
    },
    upgradeError4: {
      title: '余额不足',
      message: '请先购买足够 Alticoin ，方可升级为专业项目。',
      action: '购买'
    }
  },
  overlay: {
    title: '还差一点点...',
    message: '基于一些技术问题，暂时需要用户自行在『项目概况』页面，自行启动带GCP的三维重建。',
    ok: '前往',
    cancel: '取消'
  }
}
