export default {
  error: {
    title: '演示錯誤',
    message: '系統錯誤，請稍候重試'
  },
  success: {
    title: '演示成功',
    message: '更變已保存'
  }
}
