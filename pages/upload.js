import { App, GradientButton, LoadingWidget, StarIcon, color, theme, withAuth, withi18nSSR } from '@ald/ui'
import { Flex, Text } from 'rebass'
import React, { Component } from 'react'

import DemoHeader from '../src/components/demo-header'
import PropTypes from 'prop-types'
import UploadMeta from '../src/components/upload-project-metafile-s3'

@withAuth
class UploadMetaPage extends Component {
  static propTypes = {
    url: PropTypes.object
  }
  render () {
    const { url } = this.props
    return (
      <App
        title='Altizure | SPA Starter'
        customHeader={<DemoHeader />} option={{native: false}}>
        <Text m={5} f={30} bold children={`PID=${url.query.pid}`} />
        <UploadMeta pid={url.query.pid} />
      </App>
    )
  }
}

export default UploadMetaPage
