export default {
  title: '上傳照片到',
  titleMeta: '上傳輔助文件',
  learnMore: '了解更多',
  free: '免費項目',
  paid: '專業項目',
  uploader: {
    drop: '點擊或拖拉文件',
    edge: '雲端位置',
    init: '初次化中',
    uploading: '正在上傳',
    obtainingSTS: '連接雲端',
    skipping: '跳過已上傳文件',
    registering: '核對文件中',
    uploaded: '上傳成功',
    error: '出錯文件'
  },
  reminder: {
    tip: '請確保圖片滿足以下條件，以達最佳效果。 ',
    dos: {
      'overlap': '足夠重疊度',
      'angle': '多角度拍攝',
      'color': '統一相機設定',
      'static': '物件靜止'
    },
    donts: {
      'surface': '透明，反光，純色表面',
      'blur': '模糊，失焦圖片',
      'mark': '時間印或其他故定標記'
    },
    note: '提示',
    note0: '照片不足800萬像素會當作800萬像素計算。 希望用戶上傳高質量照片。',
    note1: '支援斷續上傳內容一樣的文件，會自動跳過已上傳的文件。 ',
    note2: '支援上傳同名字但不同內容的文件。只要文件內容不一樣，系統亦視作不同文件來上傳。例如 DJI 的照片每 1000 張便重複名字，但一起上傳亦不會互相覆蓋。 ',
    note3: '我們建議每次上傳不多過 1000 張照片。否則瀏覽器可能會出錯。 ',
    note4: '數據採集​​教學',
    note4Link: `/support/articles/tutorial_capture`
  },
  progress: '進度',
  total: '所有文件',
  uploading: '上傳中',
  processing: '處理中',
  // processing: '上传中',
  valid: '有效文件',
  invalid: '無效文件',
  detail: '詳情',
  more: '還有更多',
  no: '沒有',
  image: {
    title: '項目照片',
    hasProcessing: '尚有照片在雲端處理中。',
    confirmRemove: '確認刪除所選文件？數量一共',
    perpage: '每頁顯示',
    select: '選擇',
    confirm: '確認',
    removing: '刪除中',
    cancel: '取消',
    loadMore: '更多',
    width: '寬',
    height: '高',
    filesize: '文件大小',
    datasize: '數據大小',
    gp: '十億像素',
    unique: '獨有名字',
    checksum: '校驗碼',
    remove: '刪除',
    state: {
      title: '文件狀態',
      All: '全部',
      Pending: '等候上傳', // validated with OSS
      Uploading: '上傳中', // uploading to OSS
      Uploaded: '上傳完成', // uploaded to OSS
      Verifying: '檢查中', // after uploaded to OSS
      Valid: '有效照片', // after verfiying and OK
      Invalid: '無效文件', // after and verfied
      Copying: '傳送到重建系統', // copying from OSS to local
      Ready: '全部完成' // copied to local
    }
  },
  report: '報告問題',
  retry: '重試全部',
  switch: '換伺服器試試',
  // starttask
  gcp: '加入控制點 (GCP)',
  autoStart: '處理完成所有文件後自動開始重建程序',
  autoStartTip: '即 『上傳中』的文件數量降至 0 時開始',
  push: '開始重建',
  next: '下一步',
  // alert message
  alert: {
    refreshAlert: {
      title: '照片已更新',
      message: '請刷新頁面'
    },
    removeError: {
      title: '刪除失敗',
      message: '照片無法刪除，請稍候再試。'
    },
    pushError: {
      title: '系統錯誤',
      message: '重建任務現時無法開始。請放心，你已上傳的圖片沒有丟失，請稍候重試。'
    },
    // see graphql doc
    pushError0: {
      title: '帳戶認證錯誤',
      message: '請先登錄你的帳戶。'
    },
    pushError1: {
      title: '找不到項目',
      message: '項目不存在，無法啟動重建。'
    },
    pushError2: {
      title: '照片未準備好',
      message: '尚有照片在雲端處理中。當所有照片從雲端傳送到運算機器，方能啟動重建。',
      action: '查看所有照片'
    },
    pushError3: {
      title: '重建已經開始',
      message: '如有需要，請先停止目前的重建。'
    },
    pushError4: {
      title: '餘額不足',
      message: '請先購買足夠 Alticoin ，方可為專業項目啟動重建。',
      action: '購買'
    },
    pushError5: {
      title: '項目沒有改變',
      message: '如果輸入數據沒有改變，再次重建也不會改變。'
    },
    pushError6: {
      title: '超出免費額度',
      message: '重建無法啟動。'
    },
    pushError7: {
      title: '照片不足',
      message: '請上傳更多照片。'
    },
    pushError8: {
      title: '優惠券無效',
      message: '無法使用優惠券。'
    },
    pushError9: {
      title: '優惠券過期',
      message: '無法使用優惠券。'
    },
    uploadError: {
      title: '上傳錯誤',
      message: '文件暫時無法上傳，請稍候重試。'
    },
    reportError: {
      title: '報告問題出錯',
      message: '問題暫時無法報告給我們，請你稍後再試。或者把直接問題發到 support@altizure.com。謝謝！'
    },
    reportDone: {
      title: '報告成功',
      message: '謝謝你的報告。我們會盡快調查跟進，與你聯絡。'
    },
    upgradeError: {
      title: '系統錯誤',
      message: '項目暫時無法升級成專業，請稍候重試。'
    },
    upgradeError0: {
      title: '帳戶認證錯誤',
      message: '請先登錄你的帳戶。'
    },
    upgradeError1: {
      title: '找不到項目',
      message: '項目不存在，無法升級成專業。'
    },
    upgradeError2: {
      title: '項目已經升級',
      message: '項目已經是專業，毋須再升級。'
    },
    upgradeError3: {
      title: '項目有正在執行的任務',
      message: '請先等候任務完成。'
    },
    upgradeError4: {
      title: '餘額不足',
      message: '請先購買足夠 Alticoin ，方可升級為專業項目。',
      action: '購買'
    }
  },
  overlay: {
    title: '還差一點點...',
    message: '基於一些技術問題，暫時需要用戶自行在『項目概況』頁面，自行啟動帶 GCP 的三維重建。',
    ok: '前往',
    cancel: '取消'
  }
}
