export default {
  error: {
    title: 'Demo Error',
    message: 'Server error. Try again later.'
  },
  success: {
    title: 'Demo Success',
    message: 'Update saved.'
  }
}
