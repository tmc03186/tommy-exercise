import EN from './en/demo'
import CN from './zh-cn/demo'
import TW from './zh-tw/demo'
export default {
  en: EN,
  'zh-cn': CN,
  'zh-tw': TW
}
