export default {
  error: {
    title: '演示错误',
    message: '系统错误，请稍候重试'
  },
  success: {
    title: '演示成功',
    message: '更变已保存'
  }
}
