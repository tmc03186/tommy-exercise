import gql from 'graphql-tag'

export default gql`
  query mySelf{
    my {
      self {
        id
        name
        username
        email
        profileFace
        balance
        freeGPQuota
        groups {
          edges {
            node {
              name
            }
          }
        }
      }
    }
  }
`
