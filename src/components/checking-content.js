import { BlockLink, Box, Flex, Pre, Text, Textarea } from 'rebass'
import { GradientButton, theme } from '@ald/ui'
import React, { Component } from 'react'

import PropTypes from 'prop-types'
import TxtJson from './txtJson'

const MyFlex = Flex.extend`
`

MyFlex.defaultProps = {
  ...Flex.defaultProps,
  align: 'center',
  width: theme.contentWidth,
  m: 'auto'
}

const LargeFlex = MyFlex.extend`
  min-height: 600px;
  flex-wrap: wrap;
`

const myMainContentFlex = {
  maxHeight: '500px'
}

const MyTextarea = Textarea.extend`
`
MyTextarea.defaultProps = {
  ...Textarea.defaultProps,
  width: '80%',
  height: '80%',
  align: 'center',
  rows: '20',
  maxHeight: '400px'
}

const myScrollDiv = {
  overflow: 'scroll-y',
  maxHeight: '500px'
}

const MyStyledBox = Box.extend`
`

MyStyledBox.defaultProps = {
  ...Box.defaultProps,
  w: [1, 1, 1, 1 / 2]
}

class CheckingContent extends Component {
  static propTypes = {
    fileName: PropTypes.oneOf(['pose', 'Photos'])
  }

  state = {
    longStr: '',
    goodStr: '',
    badStr: '',
    goodStrCount: 0,
    expectedStrCount: 0
  }
  goodStr = ''
  badStr = ''
  goodStrCount = 0
  expectedStrCount = 0
  jsonString = ''

  handleClick = (e) => {
    var promise = Promise.resolve(
      this.beginSetState()
    )

    promise.then(() => {
      this.splitLine(this.state.longStr)
    })
  }

  handleChange = (e) => {
    this.setState({longStr: e.target.value})
  }

  splitLine = (str) => {
    if (this.checkEmpty(str)) return
    var lines = []
    lines = str.split('\n')
    if (this.checkEmpty(lines[ (lines.length - 1)])) {
      this.expectedStrCount = lines.length - 1
    } else this.expectedStrCount = lines.length

    lines.forEach((line, i) => {
      line = line.trim()
      this.checkLine(line, i, 4) // need change
      if (i === lines.length - 1) this.endSetState()
    })
  }

  beginSetState = () => {
    this.setState({
      goodStrCount: 0,
      badStr: '',
      expectedStrCount: 0
    })
    this.goodStr = ''
    this.badStr = ''
    this.goodStrCount = 0
    this.expectedStrCount = 0
    this.jsonString = ''
  }

  endSetState = () => {
    this.setState({
      goodStr: this.goodStr,
      goodStrCount: this.goodStrCount,
      badStr: this.badStr,
      expectedStrCount: this.expectedStrCount
    })
    let boo
    if (this.state.longStr.trim().split('\n')[2].split(' ').length > 5) boo = true
    else boo = false
  }

  checkLine = (line, index, items) => {
    if (this.checkEmpty(line)) return
    var arr = []
    arr = line.split(' ')
    if (arr.length === items) {
      if (this.checkFormat(arr)) {
        this.sucessLine(line)
      } else this.failLine(line, index)
    } else this.failLine(line, index)
  }

  sucessLine = (line) => {
    this.goodStr += line + '\n'
    this.goodStrCount++
    // console.log(Object.keys(TxtJson.poseTxt.GPS).length)
    var arr = line.split(' ')
    TxtJson.poseTxt.imageName = arr[0]
    TxtJson.poseTxt.GPS.latitude = arr[1]
    TxtJson.poseTxt.GPS.longitude = arr[2]
    TxtJson.poseTxt.GPS.altitude = arr[3]
    if (arr[4] !== null) {
      TxtJson.poseTxt.pose.roll = arr[4]
      TxtJson.poseTxt.pose.pitch = arr[5]
      TxtJson.poseTxt.pose.yaw = arr[6]
    }
    var str = JSON.stringify(TxtJson.poseTxt, null, 2)

    if (this.jsonString === '') this.jsonString += str
    else this.jsonString += ',' + str
  }

  failLine = (line, index) => {
    this.badStr += 'Line ' + index + 1 + ': ' + line + '\r\n'
  }

  checkFormat = (arr) => {
    // check .jpg .JPG
    var imgStrArr = arr[0].split('.')
    if (imgStrArr.length === 2) {
      if (['JPG', 'PNG', 'TIFF', 'jpg', 'png', 'tiff'].indexOf(imgStrArr[1]) !== -1) {
        // check int and float
        for (let i = 1; i < arr.length; i++) {
          if (isNaN(arr[i])) return false
        }
        return true
      }
    }
    return false
  }

  checkEmpty = (str) => {
    if (str === '') return true
    return false
  }

  render () {
    return (
      <LargeFlex>
        <Box w={1}>
          <Text py={2} fontWeight='bold' fontSize='20px' >Uploading {this.props.fileName + '.txt' }</Text>
        </Box>
        <MyFlex style={myMainContentFlex} align='center'>
          {/* left textarea */}
          <MyStyledBox>
            <MyTextarea value={this.state.longStr} onChange={this.handleChange} />
          </MyStyledBox>
          {/* right table */}
          <MyStyledBox>
            {/* Table */}
            <Table goodStr={this.state.goodStr} />
          </MyStyledBox>
        </MyFlex>
        {/* log box */}
        <MyFlex align='center'>
          <MyStyledBox>
            <Pre style={myScrollDiv} children={this.state.badStr} />
            <Text m={0} p={0} children={'corrected lines / imported lines:' + this.state.goodStrCount + ' / ' + this.state.expectedStrCount} />
          </MyStyledBox>
        </MyFlex>
        {/* Button bar */}
        <MyFlex align='center'>
          <GradientButton onClick={this.handleClick} children='verify' />
          <BlockLink href={'data:text/json;charset=utf-8,' + encodeURIComponent('{' + this.jsonString + '}')} download='data.json' children={<GradientButton children='downloadJson' />} />
        </MyFlex>
      </LargeFlex>
    )
  }
}

export default CheckingContent

const myTbody = {
  display: 'block',
  overflow: 'auto',
  width: '100%',
  height: '300px'
}

class Table extends CheckingContent {
  static propTypes = {
    fileName: PropTypes.oneOf(['pose', 'Photos']),
    goodStr: PropTypes.string
  }

  render () {
    let row = this.props.goodStr.trim().split('\n').map((obj) => {
      return (
        <tr>
          {obj.split(' ').map((para) => {
            return <td style={{padding: '10px'}} children={para} />
          })}
        </tr>
      )
    })
    let theadRow = (obj) => {
      return (
        obj.map((para) => {
          return <th style={{padding: '10px', width: '100%'}} children={para} />
        })
      )
    }
    return (
      <table>
        <thead style={{display: 'block'}}>
          <tr>
            {/* theadRow with props+state */}
          </tr>
        </thead>
        <tbody style={myTbody}>
          {row}
        </tbody>
      </table>
    )
  }
}
