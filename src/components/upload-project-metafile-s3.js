import {
  Box,
  Button,
  Flex,
  Text,
  Truncate
} from 'rebass'
import React, { Component } from 'react'
import { color, helper, withi18nSSR } from '@ald/ui'
import {
  compose,
  graphql,
  withApollo
} from 'react-apollo'

import Dropzone from 'react-dropzone'
import PropTypes from 'prop-types'
import {
  UploadCloud
} from 'react-feather'
import { debounce } from 'lodash'
import moment from 'moment'
import mutatePending from '../gql/mutations/upload-metafile-s3'
import { put } from 'axios'
// import ReportButton from './report-button'
import sha1sum from '../../src/lib/sha1sum'
// import { updateDroppedFiles } from '../actions'
import upload from '../../src/locales/upload'

// import { uploadError } from '../alerts/upload'

const { gray, blue5, red1 } = color
const { hasMeta } = helper

const maxRetryCount = 5

@withi18nSSR(upload)
class UploadProjectMetaWidget extends Component {
  static contextTypes = {
    store: PropTypes.object
  }
  static propTypes = {
    client: PropTypes.object,
    pid: PropTypes.string,
    bucket: PropTypes.string,
    registerUpload: PropTypes.func,
    // startUpload: PropTypes.func,
    i18n: PropTypes.object
  }
  state = {
    retriedCount: 0,
    debugMsg: ''
  }
  errorMessage = []

  log = debugMsg => this.setState({ debugMsg })
  switchDropping = (dropping) => this.setState({ dropping })
  clearErrorMessage = () => { this.errorMessage = [] }
  pushErrorMessage = msg => {
    this.errorMessage.push({
      time: moment().format('llll'),
      msg
    })
  }
  constructErrorMessage = () => {
    const { pid, bucket } = this.props
    let decoded
    try {
      decoded = window.atob(bucket)
    } catch (err) {
    }
    return JSON.stringify({
      props: { pid, bucket: decoded },
      type: 'MetaFiles',
      state: this.state,
      error: this.errorMessage
    }, null, 2)
  }
  // drop invalid files to onDrop function
  retryAll = () => {
    if (this.state.dropping) {
      return
    }
    this.setState({ retriedCount: this.state.retriedCount + 1 }, () => {
      const { getState } = this.context.store
      const invalidFiles = getState().upload.filter(i => i.state === 'Invalid').map(i => i.file)
      this.onDrop(invalidFiles, [], true)
    })
  }
  debouncedRetryAll = debounce(this.retryAll, 500)

  onDrop = async (acceptedFiles, rejectedFiles, force = false) => {
    if (this.state.dropping) {
      return
    }

    const { dispatch, getState } = this.context.store
    const { pid, bucket, i18n } = this.props
    let decoded
    try {
      decoded = window.atob(bucket)
    } catch (err) {
    }

    this.switchDropping(true)
    // quickly go through all images and give them initial state to show on UI
    // either Pending or Invalid
    let naiveChecksums = getState().upload.map(i => i.naiveChecksum)
    let all = []
    let files = []
    for (let i = 0; i < acceptedFiles.length; i++) {
      const df = acceptedFiles[i]
      this.log(`${i18n.uploader.init}... ${i + 1}/${acceptedFiles.length}`)
      try {
        // const checksum = await sha1sum(df)
        const naiveChecksum = String(df.name) + String(df.size)
        if (!naiveChecksums.find(cs => cs === naiveChecksum) || force) {
          files.push({
            file: df,
            naiveChecksum
          })
          all.push({
            file: df,
            name: df.name,
            state: 'Pending',
            naiveChecksum
          })
        }
      } catch (err) {
        console.error(err)
        // dispatch(uploadError(i18n.lang, df.name))
        all.push({
          file: df,
          name: df.name,
          state: 'Invalid'
        })
      }
    }
    // dispatch(updateDroppedFiles(all))
    this.switchDropping(false)

    // start uploading
    // let checksums = getState().upload.map(i => i.checksum)
    for (let i = 0; i < files.length; i++) {
      const { file } = files[i]
      const { name, type } = file

      // Client Side handle error and update file life cycle
      // since server is unaware of this failed image
      let registered
      try {
        // a. check hash
        const checksum = await sha1sum(file)
        const found = await hasMeta(this.props.client, pid, checksum)
        if (found) {
          this.log(`${i18n.uploader.skipping}... ${name}`)
          // dispatch(updateDroppedFiles([{ file, ...found }]))
          continue
        }

        // b. signing S3 url + register new image
        this.log(`${i18n.uploader.registering}... ${name}`)
        registered = await this.props.registerUpload({
          variables: {
            pid,
            bucket: decoded || 'Singapore',
            filename: name
          }
        })
      } catch (err) {
        console.log('err', err)
        this.pushErrorMessage(err.toString())
        // dispatch(uploadError(i18n.lang, name))
        // dispatch(updateDroppedFiles([{ file, state: 'Invalid' }]))
        continue
      }

      // Server side handle image life cycle from now on
      try {
        // d. signal the start of image uploading
        const { data: { uploadMetaFileS3 } } = registered
        const { url, file: { id } } = uploadMetaFileS3

        // update id got from server or drop it if unfound
        if (id) {
          // dispatch(updateDroppedFiles([{ file, id, state: 'Uploading' }]))
        } else {
          console.log('dropping')
          // dispatch(updateDroppedFiles([{ file, drop: true }]))
        }

        // d. actual upload
        const options = {
          headers: {
            'Content-Type': type
          }
        }
        const res = await put(url, file, options)
        if (res.status === 200) {
          this.log(`${i18n.uploader.uploaded}: ${name}`)
          // dispatch(updateDroppedFiles([{ file, id, state: 'Uploaded' }]))
        } else {
          throw new Error('upload failed')
        }
      } catch (err) {
        console.log('err', err)
        this.pushErrorMessage(err.toString())
        // dispatch(uploadError(i18n.lang, name))
        this.log(`${i18n.uploader.error}: ${name}`)
      }
    } // end of for loop
    this.log('')
  }
  render () {
    const { getState } = this.context.store
    const { i18n, pid } = this.props
    const { dropping, retriedCount } = this.state
    const invalidFiles = getState().upload.filter(i => i.state === 'Invalid')
    return (
      <Box w={1}>
        <Dropzone
          accept='text/plain'
          onDrop={(accept, reject) => this.onDrop(accept, reject)}
          disabled={dropping}
          style={{ width: '100%', borderStyle: 'dashed', cursor: 'pointer', boxSizing: 'border-box' }}>
          <Box m={5}>
            <Text center>
              <UploadCloud size={32} m={2} color={dropping ? gray : blue5} />
            </Text>
            <Truncate f={5} mb={1} center>
              {this.state.debugMsg || i18n.uploader.drop}
            </Truncate>
          </Box>
        </Dropzone>
        {(this.errorMessage.length > 0 || invalidFiles.length > 0) &&
          <Flex w={1} p={3} bg={red1} justify='flex-end'>
            {invalidFiles.length > 0 &&
              <Button
                ml={2}
                disabled={dropping}
                children={`${i18n.retry} (${invalidFiles.length})`}
                onClick={this.retryAll} />
            }
          </Flex>
        }
      </Box>
    )
  }
}

const options = {
  refetchQueries: [
    'projectMetaFiles'
  ]
}

export default compose(
  withApollo,
  graphql(mutatePending, { name: 'registerUpload', options })
)(UploadProjectMetaWidget)
