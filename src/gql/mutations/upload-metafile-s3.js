import gql from 'graphql-tag'

export default gql`
  mutation (
    $pid: ID!,
    $bucket: BucketS3!,
    $filename: String!
  ) {
    uploadMetaFileS3(
      pid: $pid,
      bucket: $bucket,
      filename: $filename
    ) {
      url
      file {
        id
        state
        name
        filename
        filesize
        date
        checksum
        error
      }
    }
  }
`
