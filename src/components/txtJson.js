export default {
  'poseTxt': {
    'imageName': '',
    'GPS': {
      'latitude': '',
      'longitude': '',
      'altitude': ''
    },
    // '_comment': 'pose is optional',
    'pose': {
      'roll': '',
      'pitch': '',
      'yaw': ''
    }
  },
  'cameraTxt': {
    'imageName': '',
    'focalLength': '',
    'imageCentre': {
      'x0': '',
      'y0': ''
    },
    'index': ''
  },
  'groupTxt': {
    'imageName': '',
    'groupId': '',
    'index': ''
  }
}
