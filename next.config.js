const withTM = require('@weco/next-plugin-transpile-modules')
const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const path = require('path')
const { parsed: localEnv } = require('dotenv').config()
const webpack = require('webpack')

// next.config.js
module.exports = withTM({
  webpack: (config, { buildId, dev }) => {
    // Perform customizations to webpack config
    config.module.noParse = /(mapbox-gl)\.js$/
    // Important: return the modified config

    // customized polyfill which is needed for dependency package
    // ref: https://github.com/zeit/next.js/pull/3568
    const originalEntry = config.entry
    config.entry = async () => {
      const entries = await originalEntry()
      // console.log(entries)
      if (entries['main.js']) {
        entries['main.js'].unshift('./node_modules/@ald/ui/dist/polyfill.js')
      }
      return entries
    }

    // plugins
    config.plugins.push(
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /zh-tw|zh-cn/),
      new webpack.EnvironmentPlugin(localEnv)
    )
    if (process.env.ANALYZE) {
      console.log('bundle analyzer turn on')
      config.plugins.push(new BundleAnalyzer({
        analyzerMode: 'static',
        reportFilename: path.resolve(__dirname, 'reoprt.html'),
        generateStatsFile: true,
        statsFilename: path.resolve(__dirname, 'stats.json')
      }))
    }

    return config
  },
  transpileModules: [
    '@ald/ui'
  ],
  exportPathMap: function () {
    return {
      '/': { page: '/' },
      '/dashboard': { page: '/dashboard' },
      '/about': { page: '/about' }
    }
  }
})
