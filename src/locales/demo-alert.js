import EN from './en/demo-alert'
import CN from './zh-cn/demo-alert'
import TW from './zh-tw/demo-alert'
export default {
  en: EN,
  'zh-cn': CN,
  'zh-tw': TW
}
