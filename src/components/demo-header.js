import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  withi18nSSR,
  GradientButton,
  Header,
  LangPicker,
  UserWidget
} from '@ald/ui'
import demo from '../locales/demo'

@withi18nSSR(demo)
class DemoHeader extends Component {
  static contextTypes = {
    hostEndPoint: PropTypes.string
  }
  static propTypes = {
    hostEndPoint: PropTypes.string,
    i18n: PropTypes.object
  }
  alert = () => window.alert('Hello')
  render () {
    const {
      i18n,
      hostEndPoint = this.context.hostEndPoint || 'https://www.altizure.com'
    } = this.props
    return (
      <Header
        logoSection={{
          data: i18n.title
        }}
        navigationSection={{
          data: [
            {
              native: true,
              href: '/',
              text: i18n.home
            },
            {
              href: hostEndPoint,
              text: i18n.altizure
            },
            {
              widget: <GradientButton mx={0} onClick={this.alert} children={i18n.notifySuccess} />
            }
          ]
        }}
        controlSection={{
          data: [
            <LangPicker />,
            <UserWidget />
          ]
        }}
        sideMenuSection={{
          data: [
            {
              widget: <GradientButton mx={0} w={1} onClick={this.alert} children={i18n.notifySuccess} />
            },
            {
              native: true,
              href: '/',
              text: i18n.home
            },
            {
              href: hostEndPoint,
              text: i18n.altizure
            },
            {
              widget: <LangPicker sideMenu />
            }
          ]
        }}
      />
    )
  }
}
export default DemoHeader
