import demoAlert from '../locales/demo-alert'
import { success as sus, error as err } from 'react-notification-system-redux'

const alert = (lang, type) => {
  const { title, message } = demoAlert[lang][type]
  return {
    title,
    message,
    position: 'tc',
    autoDismiss: 5
  }
}

export const error = lang => err(alert(lang, 'error'))
export const success = lang => sus(alert(lang, 'success'))
